

import mysql.connector
import pymongo
import time

def fetchSqlData(): 
    try:

        myDB = mysql.connector.connect(
        host="localhost",
        user="root",
        password="Bhargav@2002",
        )

        mySqlCursor = myDB.cursor()

        mySqlCursor.execute("CREATE DATABASE IF NOT EXISTS python_assignment")
        mySqlCursor.execute("USE python_assignment")

        mySqlCursor.execute("SHOW TABLES")
        tables = mySqlCursor.fetchall()
        table_exists = any("employee" in table for table in tables)
        if not table_exists:
            mySqlCursor.execute("CREATE TABLE IF NOT EXISTS employee (id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), age INT, desg VARCHAR(255))")

            sql = "INSERT INTO employee (name, age, desg) VALUES (%s, %s`, %s)"
            val = [
            ('Peter', 20, 'SDE 1'),
            ('Amy', 21, 'Intern'),
            ('Hannah', 25, 'Manager'),
            ('Michael', 35, 'CTO'),
            ('Sandy', 30, 'SSE'),
            ('Betty', 28, 'SDE 3'),
            ('Richard', 23, 'SDE 2'),
            ('Susan', 19, 'Intern')
            ]

            mySqlCursor.executemany(sql, val)
            myDB.commit()

            for x in mySqlCursor:
                print(x)

        mySqlCursor.execute("SELECT * FROM employee")
        migrateData = mySqlCursor.fetchall()
        return migrateData

    except mysql.connector.Error as e:
        print(e)

    finally:
        mySqlCursor.close()
        myDB.close()





def migrateToMongo():
    dataToMigrate = fetchSqlData()
    print(dataToMigrate)

    try:

        myclient = pymongo.MongoClient("mongodb://localhost:27017/")

        myDB = myclient['python_assignment']
        myCol = myDB['employee']

        start_time = time.time()
        migratedDataCount=0
        for data in dataToMigrate:
            try:
                document = {
                    "name": data[0],
                    "age": data[1],
                    "designation": data[2]
                }

                myCol.insert_one(document)
                migratedDataCount+=1

            except Exception as e:
                print(f"Error: {e}")
        
        end_time = time.time()
        print("Number of Records migrated : ", migratedDataCount)
        print("Number of Records Failed to Migrate : ", len(dataToMigrate)-migratedDataCount)
        print("Total Time Taken : ", end_time - start_time)

    except Exception as e:
        print(f"Error: {e}")
    finally:
        myclient.close()